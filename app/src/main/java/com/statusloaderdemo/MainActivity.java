package com.statusloaderdemo;

import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.Snackbar;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.support.design.widget.NavigationView;
import android.support.v4.view.GravityCompat;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.ProgressBar;

import com.statusloaderdemo.adapters.ListAdapter;
import com.statusloaderdemo.data.RecordsDataProvider;
import com.statusloaderdemo.data.SLDDatabaseHelper;
import com.statusloaderdemo.list.EndlessRecyclerViewScrollListener;

public class MainActivity extends AppCompatActivity
        implements NavigationView.OnNavigationItemSelectedListener {

    private ProgressBar mProgressBar;

    private RecyclerView mRecyclerView;
    private ListAdapter mListAdapter;
    private RecordsDataProvider mRecordsDataProvider;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        ActionBarDrawerToggle toggle = new ActionBarDrawerToggle(
                this, drawer, toolbar, R.string.navigation_drawer_open, R.string.navigation_drawer_close);
        drawer.setDrawerListener(toggle);
        toggle.syncState();

        NavigationView navigationView = (NavigationView) findViewById(R.id.nav_view);
        navigationView.setNavigationItemSelectedListener(this);

        mProgressBar = (ProgressBar) findViewById(R.id.progressBar);

        SLDDatabaseHelper dbHelper = new SLDDatabaseHelper(this);

        mRecordsDataProvider = new RecordsDataProvider(dbHelper);

        mRecyclerView = (RecyclerView) findViewById(R.id.list);
        RecyclerView.LayoutManager layoutManager = new LinearLayoutManager(this, LinearLayoutManager.VERTICAL, false);

        mListAdapter = new ListAdapter(this);

        mRecyclerView.setLayoutManager(layoutManager);
        mRecyclerView.setAdapter(mListAdapter);

        EndlessRecyclerViewScrollListener recyclerViewScrollListener = new EndlessRecyclerViewScrollListener(layoutManager) {
            @Override
            public void onScrollForwardLoadMore(EndlessRecyclerViewScrollListener scrollListener) {
                mListAdapter.loadMoreData();
            }
        };
        mRecyclerView.addOnScrollListener(recyclerViewScrollListener);

        navigationView.setCheckedItem(R.id.all_items);
        setTitle(R.string.toolbar_title_all_items);
    }

    @Override
    public void onBackPressed() {
        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        if (drawer.isDrawerOpen(GravityCompat.START)) {
            drawer.closeDrawer(GravityCompat.START);
        } else {
            super.onBackPressed();
        }
    }

    @SuppressWarnings("StatementWithEmptyBody")
    @Override
    public boolean onNavigationItemSelected(MenuItem item) {
        // Handle navigation view item clicks here.
        int id = item.getItemId();

        if (id == R.id.all_items) {
            mListAdapter.setAllItemsMode();
            setTitle(R.string.toolbar_title_all_items);
        } else if (id == R.id.n_items) {
            mListAdapter.setNMode();
            setTitle(R.string.toolbar_title_n_items);
        }

        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        drawer.closeDrawer(GravityCompat.START);
        return true;
    }

    // Returns Records data provider to get access to database
    public RecordsDataProvider getDataProvider(){
        return mRecordsDataProvider;
    }

    // Shows progress bar (loading)
    public void showProgressBar(){
        mRecyclerView.setVisibility(View.GONE);
        mProgressBar.setVisibility(View.VISIBLE);
    }

    // Hides progress bar (loading finished)
    public void hideProgressBar(){
        mProgressBar.setVisibility(View.GONE);
        mRecyclerView.setVisibility(View.VISIBLE);
    }
}

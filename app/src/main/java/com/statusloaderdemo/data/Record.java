package com.statusloaderdemo.data;


public class Record {

    private long id;
    private String name;
    private String comment;
    private String number;
    private String ip;

    public Record(String name, String comment, String number, String ip){
        this.name = name;
        this.comment = comment;
        this.number = number;
        this.ip = ip;
    }

    public Record(long id, String name, String comment, String number, String ip){
        this.id = id;
        this.name = name;
        this.comment = comment;
        this.number = number;
        this.ip = ip;
    }

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getComment() {
        return comment;
    }

    public void setComment(String comment) {
        this.comment = comment;
    }

    public String getNumber() {
        return number;
    }

    public void setNumber(String number) {
        this.number = number;
    }

    public String getIp() {
        return ip;
    }

    public void setIp(String ip) {
        this.ip = ip;
    }
}

package com.statusloaderdemo.data;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteException;
import android.database.sqlite.SQLiteOpenHelper;
import android.util.Log;

import com.statusloaderdemo.data.SLDContract.*;

import java.util.List;

public class SLDDatabaseHelper extends SQLiteOpenHelper {

    private static final String LOG_TAG = "SLDDatabaseHelper";

    private static final String DATABASE_NAME = "statusLoader.db";
    private static final int DATABASE_VERSION = 1;

    public SLDDatabaseHelper(Context context){
        super(context, DATABASE_NAME, null, DATABASE_VERSION);
    }

    @Override
    public void onCreate(SQLiteDatabase db) {
        final String CREATE_TABLE_RECORDS = "CREATE TABLE " + RecordEntry.TABLE_NAME + " ("
                + RecordEntry._ID + " INTEGER PRIMARY KEY AUTOINCREMENT,"
                + RecordEntry.COLUMN_NAME + " TEXT NOT NULL,"
                + RecordEntry.COLUMN_COMMENT + " TEXT NOT NULL,"
                + RecordEntry.COLUMN_NUMBER + " TEXT NOT NULL,"
                + RecordEntry.COLUMN_IP + " TEXT NOT NULL" + ");";

        db.execSQL(CREATE_TABLE_RECORDS);
    }

    @Override
    public void onUpgrade(SQLiteDatabase db, int i, int i1) {
        db.execSQL("DROP TABLE IF EXISTS " + RecordEntry.TABLE_NAME);

        onCreate(db);
    }

    // Insert generated (loaded from server) data to database
    public boolean insertRecords(List<Record> records){
        SQLiteDatabase db = this.getWritableDatabase();

        boolean succeed = false;

        db.beginTransaction();
        try {

            for (int i = 0; i < records.size(); i ++){
                ContentValues values = new ContentValues();
                values.put(RecordEntry.COLUMN_NAME, records.get(i).getName());
                values.put(RecordEntry.COLUMN_COMMENT, records.get(i).getComment());
                values.put(RecordEntry.COLUMN_NUMBER, records.get(i).getNumber());
                values.put(RecordEntry.COLUMN_IP, records.get(i).getIp());

                /*long id = */db.insert(RecordEntry.TABLE_NAME, null, values);
            }

            db.setTransactionSuccessful();
            succeed = true;
        } catch (Exception e){
            Log.e(LOG_TAG, "Insert records exception: " + e.getMessage());
        } finally {
            db.endTransaction();
        }

        return succeed;
    }

    // Get more records from database for all items mode
    public Cursor getMoreRecords(long id){
        SQLiteDatabase db = this.getReadableDatabase();

        return db.query(RecordEntry.TABLE_NAME,
                null,
                RecordEntry._ID + " > ?",
                new String[]{ String.valueOf(id) },
                null,
                null,
                null,
                String.valueOf(RecordsDataProvider.LOAD_MORE_ITEMS_COUNT));
    }

    // Get more records from database for NMode
    public Cursor getMoreRecordsStartingWithN(long id){
        SQLiteDatabase db = this.getReadableDatabase();

        return db.query(RecordEntry.TABLE_NAME,
                null,
                RecordEntry._ID + " > ? AND " + RecordEntry.COLUMN_NAME + " LIKE 'N%'",
                new String[]{ String.valueOf(id) },
                null,
                null,
                null,
                String.valueOf(RecordsDataProvider.LOAD_MORE_ITEMS_COUNT));
    }

    // Clear Record table
    public void clearRecords(){
        SQLiteDatabase db = this.getWritableDatabase();

        db.delete(RecordEntry.TABLE_NAME, null, null);
    }
}

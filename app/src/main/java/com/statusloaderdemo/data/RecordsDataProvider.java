package com.statusloaderdemo.data;

import android.database.Cursor;
import android.os.AsyncTask;

import com.statusloaderdemo.adapters.ListAdapter;

import java.util.ArrayList;
import java.util.List;
import java.util.Random;

public class RecordsDataProvider {

    // Amount of records to load from database per request
    public static final int LOAD_MORE_ITEMS_COUNT = 60;

    private static SLDDatabaseHelper mDbHelper;

    public RecordsDataProvider(SLDDatabaseHelper dbHelper){
        mDbHelper = dbHelper;
    }

    // Load more records from database
    // lastItemID - id of last item already loaded from database
    // mode - specific data mode (currently all items or NMode)
    public List<Record> getMoreRecords(long lastItemID, int mode){
        List<Record> records = new ArrayList<>();

        Cursor recordsCursor;

        switch (mode){
            case ListAdapter.ALL_ITEMS_MODE:
                recordsCursor = mDbHelper.getMoreRecords(lastItemID);
                break;
            case ListAdapter.STARTING_WITH_N_ITEMS_MODE:
                recordsCursor = mDbHelper.getMoreRecordsStartingWithN(lastItemID);
                break;
            default:
                recordsCursor = null;
                break;
        }

        if(recordsCursor == null || recordsCursor.getCount() == 0)
            return null;

        for(int i = 0; i < recordsCursor.getCount(); i++){
            recordsCursor.moveToPosition(i);

            records.add(new Record(
                    recordsCursor.getLong(recordsCursor.getColumnIndex(SLDContract.RecordEntry._ID)),
                    recordsCursor.getString(recordsCursor.getColumnIndex(SLDContract.RecordEntry.COLUMN_NAME)),
                    recordsCursor.getString(recordsCursor.getColumnIndex(SLDContract.RecordEntry.COLUMN_COMMENT)),
                    recordsCursor.getString(recordsCursor.getColumnIndex(SLDContract.RecordEntry.COLUMN_NUMBER)),
                    recordsCursor.getString(recordsCursor.getColumnIndex(SLDContract.RecordEntry.COLUMN_IP)
                    )));
        }

        return records;
    }

    // Asynchronous task to generate random records to database
    // Instead of this will be loading data from server
    public static class RegenerateRecordsTask extends AsyncTask<Void, Void, Void>{

        private ListAdapter mListAdapter;

        public RegenerateRecordsTask(ListAdapter listAdapter){
            mListAdapter = listAdapter;
        }

        // Main functionality of task (executes on the background)
        @Override
        protected Void doInBackground(Void... voids) {

            List<Record> newRecords = new ArrayList<>();

            Random r = new Random();
            final String alphabet = "ABCDEFGHIJKLMNOPQRSTUVWXYZ";
            final String numbers = "0123456789";

            final String[] names = {"Nick", "Lisa", "Alexander", "Matt", "Georgy", "Nataly"};

            String name;

            String comment = "";
            for(int i = 0; i < 35; i++){
                comment += alphabet.charAt(r.nextInt(alphabet.length()));
            }

            String number = "";
            for(int i = 0; i < 10; i++){
                number += numbers.charAt(r.nextInt(numbers.length()));
            }

            String ip = "";
            for(int i = 0; i < 7; i++){
                ip += numbers.charAt(r.nextInt(numbers.length()));
                if(i == 2 || i == 5 || i == 6)
                    ip += ".";
            }

            for(int i = 0; i < 6000; i++){
                name = names[r.nextInt(names.length)];
                newRecords.add(new Record(
                        name + i,
                        comment + i,
                        number + i,
                        ip + i));
            }

            mDbHelper.clearRecords();
            mDbHelper.insertRecords(newRecords);

            return null;
        }

        // Fires when task finished (has access to UI thread)
        @Override
        protected void onPostExecute(Void aVoid) {
            mListAdapter.reloadData();
        }
    }
}

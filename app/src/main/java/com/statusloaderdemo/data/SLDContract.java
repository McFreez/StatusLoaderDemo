package com.statusloaderdemo.data;

import android.provider.BaseColumns;

public class SLDContract {

    // Record table columns in database
    public static final class RecordEntry implements BaseColumns{

        public static final String TABLE_NAME = "record";

        public static final String COLUMN_NAME = "name";
        public static final String COLUMN_COMMENT = "comment";
        public static final String COLUMN_NUMBER = "number";
        public static final String COLUMN_IP = "ip";
    }
}

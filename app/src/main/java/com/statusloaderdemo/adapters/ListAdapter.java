package com.statusloaderdemo.adapters;

import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.statusloaderdemo.MainActivity;
import com.statusloaderdemo.R;
import com.statusloaderdemo.data.Record;
import com.statusloaderdemo.data.RecordsDataProvider;

import java.util.List;

public class ListAdapter extends RecyclerView.Adapter<ListAdapter.ViewHolder> {

    // Available modes
    public static final int ALL_ITEMS_MODE = 0;
    public static final int STARTING_WITH_N_ITEMS_MODE = 1;

    private List<Record> mRecords;
    private RecordsDataProvider mDataProvider;
    private MainActivity mActivity;

    // Is all data shown
    private boolean endOfTheList = false;
    // Current mode
    private int mMode = ALL_ITEMS_MODE;

    public ListAdapter(MainActivity activity){
        mDataProvider = activity.getDataProvider();
        mActivity = activity;

        mRecords = mDataProvider.getMoreRecords(0, mMode);
        if(mRecords == null || mRecords.size() == 0) {
            activity.showProgressBar();
            RecordsDataProvider.RegenerateRecordsTask task = new RecordsDataProvider.RegenerateRecordsTask(this);
            task.execute();
        }
        else
            activity.hideProgressBar();
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {

        int layoutId = R.layout.list_item;
        LayoutInflater inflater = LayoutInflater.from(mActivity);
        boolean shouldAttachToParentImmediately = false;

        View view = inflater.inflate(layoutId, parent, shouldAttachToParentImmediately);
        ViewHolder viewHolder = new ViewHolder(view);

        return viewHolder;
    }

    @Override
    public void onBindViewHolder(ViewHolder holder, int position) {
        holder.bind(mRecords.get(position));
    }

    @Override
    public int getItemCount() {
        return mRecords.size();
    }

    // Reload data from database in selected mode
    public void reloadData(){
        if(mRecords != null)
            mRecords.clear();

        mRecords = mDataProvider.getMoreRecords(0, mMode);

        notifyDataSetChanged();
        mActivity.hideProgressBar();

        endOfTheList = false;
    }

    // Load more data from database when scrolling down
    public void loadMoreData(){

        if(endOfTheList)
            return;

        long lastItemID;

        if(mRecords == null || mRecords.size() == 0)
            lastItemID = 0;
        else
            lastItemID = mRecords.get(mRecords.size() - 1).getId();

        List<Record> newRecords = mDataProvider.getMoreRecords(lastItemID, mMode);

        if(newRecords == null) {
            endOfTheList = true;
            return;
        } else
            if(newRecords.size() < RecordsDataProvider.LOAD_MORE_ITEMS_COUNT){
                endOfTheList = true;
            }

        mRecords.addAll(newRecords);
        notifyDataSetChanged();
    }

    // Change mode to show records containing names starting with N
    public void setNMode(){
        mMode = STARTING_WITH_N_ITEMS_MODE;
        reloadData();
    }

    // Change mode to show all records
    public void setAllItemsMode(){
        mMode = ALL_ITEMS_MODE;
        reloadData();
    }

    // Holder of list item
    class ViewHolder extends RecyclerView.ViewHolder{

        TextView mNameTextView;
        TextView mCommentTextView;
        TextView mDetailsTextView;

        public ViewHolder(View view) {
            super(view);

            mNameTextView = view.findViewById(R.id.name);
            mCommentTextView = view.findViewById(R.id.comment);
            mDetailsTextView = view.findViewById(R.id.details);
        }

        // bind list item with data
        void bind(Record record){
            mNameTextView.setText(record.getName());
            mCommentTextView.setText(record.getComment());
            mDetailsTextView.setText(record.getNumber() + " - " + record.getIp());
        }
    }
}
